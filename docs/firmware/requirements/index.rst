Hardware and Software
#####################
.. toctree::
    :maxdepth: 2

    supported_platforms
    hard_requirements
